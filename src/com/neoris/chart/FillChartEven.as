package com.neoris.chart
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	public class FillChartEven extends Event
	{
		
		public var chartData:ArrayCollection;
		
		public static const FILL_CHART:String ="fillChartData";
		public function FillChartEven(chartData:ArrayCollection, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(FILL_CHART, bubbles, cancelable);
			this.chartData = chartData;
		}
	}
}