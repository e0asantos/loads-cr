package com.cemex.rms.FlashIsland.impl
{
	import mx.collections.ArrayCollection;

	public interface IContext
	{
		 function get dataSource():ArrayCollection;
		
		 function get dataSourceMaxPlant():ArrayCollection;
		
		 function set dataSource(value:ArrayCollection):void;
		
		 function set dataSourceMaxPlant(value:ArrayCollection):void;
		
		 function addEventListener(type:String,listener:Function, useCapture:Boolean=false,priority:int=0,useWeakReference:Boolean=false):void;
		
	}
}