package com.cemex.rms.componentes
{
	import flash.events.MouseEvent;
	
	import com.greensock.TweenLite;

	public class VerticalTabNavigatorChart extends VerticalTabNavigator
	{
		
		public function VerticalTabNavigatorChart()
		{
			super();
			this.addEventListener(MouseEvent.MOUSE_OVER,canvas1_mouseOverHandler);
			this.addEventListener(MouseEvent.MOUSE_OUT,listaPlantas_mouseOutHandler);
			
			
		}
		protected function canvas1_mouseOverHandler(event:MouseEvent):void
		{
			// TODO Auto-generated method stub
			//this.width=150;
			TweenLite.to(this,.2,{width:150});
		}
		
		
		protected function listaPlantas_mouseOutHandler(event:MouseEvent):void
		{
			// TODO Auto-generated method stub
			//this.width=17;
			TweenLite.to(this,.2,{width:17});
		}

	}
}