package com.cemex.rms.utils
{
	import com.cemex.rms.tags.tagsSingleton;
	
	import mx.collections.ArrayCollection;

	public class ChartUtil
	{
		public function ChartUtil()
		{
		}
		public static function getStatePedido(p_objnr:String):int{
			var lv_status:int = 100;	
			switch(p_objnr){
				case "CMPO" :
					lv_status = 0;
					break;
				case "CNCO":
					lv_status = 1;
					break;
				case "NOST":
				case "ASSG":
					lv_status = 2;
					break;
				case "TBCL":
					lv_status = 3;
					break;
				case "CMPL":
					lv_status = 4;
					break;
				case "NOPA":
					lv_status = 5;
					break;
				case "BLCK":
				case "TJST":
				
					lv_status = 6;
					break;
				case "OJOB":
					lv_status = 7;
					break;
				case "UNLD":
					lv_status = 8;
					break;
				case "SIML":
					lv_status = 9;
					break;
				case "LDNG":
					lv_status = 10;
					break;
				case "SYST":
					lv_status = 11;
					break;
				case "RNGT":
					lv_status = 12;
					break;
			}
			return lv_status;				
		}
		public static var _completed:Array=[];
		public static function isCompleted(status:String):Boolean{
			return _completed.indexOf(status)>-1;
		}
		public static var _confirmed:Array=[];
		public static function isConfirmed(status:String):Boolean{
			return _confirmed.indexOf(status)>-1;
		}
		public static var _progress:Array=[];
		public static function isProgress(status:String):Boolean{
			return _progress.indexOf(status)>-1;
		}
		public static var _pending:Array=[];
		public static function isPending(status:String):Boolean{
			return _pending.indexOf(status)>-1;
		}
		public static var _cancelled:Array=[];
		public static function isCancelled(status:String):Boolean{
			return _cancelled.indexOf(status)>-1;
		}
		
		public static var _blocked:Array=[];
		public static function isBlocked(status:String):Boolean{
			//_blockedNonStatic=_blocked;
			trace("Tipo:" +status+"valor "+(_blocked.indexOf(status)>-1).toString());
			return _blocked.indexOf(status)>-1;
		}
		
		/*public  function loquesea():void{
			_blockedNonStatic=_blocked;
		}*/
		public static function getItemPartialDetailValue(ob_loadph:Object):String{
			var lv_tooltip:String = null;
			var lv_datgb:String = "";
			var lv_ezeit:String = "";
			var lv_lddat:String = "";
			var lv_lduhr:String = "";
			var lv_ualbg:String = "";
			var lv_werks:String = "";
			var lv_zzwerkool:String = "";
			var lv_vbeln:String = "";
			var lv_posnr:String = "";
			var lv_nameobj:String = "";
			var lv_matnr:String = "";
			var lv_atwrt:String = "";
			var lv_kwmeng:String = "";
			var lv_objnr:String = "";
			
			if(ob_loadph["DATGB"]!=null){
				lv_datgb = ob_loadph["DATGB"].toString();
			}
			
			if(ob_loadph["EZEIT"]!=null){
				lv_ezeit = ob_loadph["EZEIT"].toString().substr(0,-2);
			}
			
			if(ob_loadph["LDDAT"]!=null){
				lv_lddat = ob_loadph["LDDAT"].toString().substr(0,-5);
			}
			
			if(ob_loadph["LDUHR"]!=null){
				lv_lduhr = ob_loadph["LDUHR"].toString().substr(0,-2);
			}
			
			if( ob_loadph["UALBG"]!=null){
				lv_ualbg =  ob_loadph["UALBG"].toString();
			}
			
			if( ob_loadph["WERKS"]!=null){
				lv_ualbg = ob_loadph["WERKS"].toString();
			}
			
			if(ob_loadph["ZZ_WERKOOL"]!=null){
				lv_zzwerkool = ob_loadph["ZZ_WERKOOL"].toString();
			}			
			if(ob_loadph["VBELN"]!=null){
				lv_vbeln = ob_loadph["VBELN"].toString();
			}
			
			if(ob_loadph["POSNR"]!=null){
				lv_posnr = ob_loadph["POSNR"].toString();
			}
			
			if(ob_loadph["NAMEOBJ"]!=null){
				lv_nameobj = ob_loadph["NAMEOBJ"].toString();
			}
			
			if(ob_loadph["MATNR"]!=null){
				lv_matnr = ob_loadph["MATNR"].toString();
			}
			
			if(ob_loadph["ATWRT"]!=null){
				lv_atwrt = ob_loadph["ATWRT"].toString();
			}
			
			if(ob_loadph["KWMENG"]!=null){
				lv_kwmeng = ob_loadph["KWMENG"].toString();
			}
			
			if(ob_loadph["OBJNR"]!=null){
				lv_objnr = ob_loadph["OBJNR"].toString();
			}
			
			lv_tooltip = lv_datgb + "\n";
			lv_tooltip = lv_tooltip + lv_ezeit.substr(0,lv_ezeit.indexOf("GMT")) + "\n";
			//Alert.show(lv_ezeit.substr(0,lv_ezeit.indexOf("GMT")),"texto");
			lv_tooltip = lv_tooltip + lv_lddat.substr(0,lv_lddat.indexOf("GMT")).substr(0,-9) + "\n";
			lv_tooltip = lv_tooltip + lv_lduhr.substr(0,lv_lduhr.indexOf("GMT")) + "\n";
			//lv_tooltip = lv_tooltip + lv_ualbg + ":\n";//
			lv_tooltip = lv_tooltip + lv_werks + "\n";//
			lv_tooltip = lv_tooltip + lv_zzwerkool + "\n";//
			lv_tooltip = lv_tooltip + lv_nameobj + "\n";//
			lv_tooltip = lv_tooltip + lv_vbeln + "\n";//
			lv_tooltip = lv_tooltip + lv_posnr + "\n";//
			
			lv_tooltip = lv_tooltip + lv_matnr + "\n";
			//lv_tooltip = lv_tooltip + lv_atwrt + ":\n";
			lv_tooltip = lv_tooltip + lv_kwmeng + "\n";
			lv_tooltip = lv_tooltip + lv_objnr;
			
			return lv_tooltip;
			
		}
		public static function filtrarPlantas(dataSource:ArrayCollection,propiedad:String,valor:String):ArrayCollection{
			var regresa:ArrayCollection=new ArrayCollection();
			for(var q:int=0;q<dataSource.length;q++){
				if(dataSource.getItemAt(q).hasOwnProperty(propiedad)){
					if(dataSource.getItemAt(q)[propiedad]==valor){
						regresa.addItem(dataSource.getItemAt(q));
					}
				}
			}	
			
			return regresa;
		}
		public static function getItemPartialDetail(ob_loadph:Object):String{
			var lv_tooltip:String = null;
			
			
			var lv_datgb_desc:String = "-Delivery Date"; 
			var lv_ezeit_desc:String = "-Delivery Time"; 
			var lv_lddat_desc:String = "-Loading Date"; 
			var lv_lduhr_desc:String = "-Loading Time"; //
			var lv_werks_desc:String = "-Plant"; //
			var lv_zzwerkool_desc:String = "-Optimal Plant";// 
			var lv_name_desc:String = "-Job Site Name"; //
			var lv_vbeln_desc:String = "-Order No."; //
			var lv_posnr_desc:String = "-Load No.";// 
			var lv_matnr_desc:String = "-Material"; 
			var lv_kwmeng_desc:String = "-Volume";  
			var lv_objnr_desc:String = "-Status"; 
			
			
			lv_tooltip = lv_datgb_desc + ":\n";
			lv_tooltip = lv_tooltip + lv_ezeit_desc + ":\n";
			lv_tooltip = lv_tooltip + lv_lddat_desc + ":\n";
			lv_tooltip = lv_tooltip + lv_lduhr_desc + ":\n";
			//lv_tooltip = lv_tooltip + lv_ualbg_desc + ":\n";
			lv_tooltip = lv_tooltip + lv_werks_desc + ":\n";
			lv_tooltip = lv_tooltip + lv_zzwerkool_desc + ":\n";
			lv_tooltip=lv_tooltip+lv_name_desc+":\n";
			lv_tooltip = lv_tooltip + lv_vbeln_desc + ":\n";
			lv_tooltip = lv_tooltip + lv_posnr_desc + ":\n";
			//lv_tooltip = lv_tooltip + lv_nameobj_desc + ":\n";
			lv_tooltip = lv_tooltip + lv_matnr_desc + ":\n";
			//lv_tooltip = lv_tooltip + lv_atwrt_desc + ":\n";
			lv_tooltip = lv_tooltip + lv_kwmeng_desc + ":\n";
			lv_tooltip = lv_tooltip + lv_objnr_desc;
			
			return lv_tooltip;
		}
		public static function getArrayCapacidadMaxima(p_plant:String,collection:ArrayCollection):Array{
			var arr_cap_max:Array = new Array(-100,-100,-100,-100,-100,-100,-100,-100,-100,-100,-100,-100,-100,-100,-100,-100,-100,-100,-100,-100,-100,-100,-100,-100);
			var dataSource:ArrayCollection = collection;
			var ob_vehicle:Object 	= null;
			var lv_iter:int			= 1;
			for(var i:Number=0; i<dataSource.length; i++){
				/*ob_vehicle = dataSource.getItemAt(i);
				if(ob_vehicle["WERKS"].toString() == p_plant ){
					arr_cap_max[parseInt(ob_vehicle["HENDLD"].toString().substring(0,2))] = parseInt(ob_vehicle["TOTVEHI"].toString());
				}*/			
			}
			//correxion
			var primero:int=-1000;
			var prim:int=0;
			for(var e:int=0;e<arr_cap_max.length;e++){
				if(primero==-1000 && arr_cap_max[e]!=-100){
					primero=arr_cap_max[e];
					prim=e+1;
				}
			}
			var ultimo:int=-1000;
			var ult:int=0;
			for(e=arr_cap_max.length-1;e>-1;e--){
				if(ultimo==-1000 && arr_cap_max[e]!=-100){
					ultimo=arr_cap_max[e];
					ult=e-1;
				}
			}
			for(e=0;e<prim;e++){
				arr_cap_max[e]=primero;
			}
			for(e=arr_cap_max.length-1;e>ult;e--){
				arr_cap_max[e]=ultimo;
			}
			return arr_cap_max;
		}
		public static function getLabelsOrderDetail():String{
			var lv_label:String = null;
			
			var ob_detail:Object = null;
			var ob_line:Object = null;
			
			var lv_datgb_desc:String = "Current date for start of shippment";  /**loading time**/
			var lv_ezeit_desc:String = "Arrival time";
			var lv_uatbg_desc:String = "Actual transport start time";
			var lv_lddat_desc:String = "Loading Date";
			var lv_lduhr_desc:String = "Loading Time";
			var lv_ualbg_desc:String = "Actual loading start time";
			var lv_werks_desc:String = "Plant( Own or External )";
			var lv_name_desc:String  = "Name";
			var lv_zzwerkool_desc:String = "Optimun Plant";
			var lv_vbeln_desc:String = "Sales document";
			var lv_posnr_desc:String = "Sales document item";
			var lv_namecust_desc:String = "Name";
			var lv_nameobj_desc:String = "name";
			var lv_arktx_desc:String = "Short text for sales order item";
			var lv_matnr_desc:String = "Material Number";
			var lv_atwrt_desc:String = "Characteristic value";
			var lv_kwmeng_desc:String = "Cumulative order quantity in sales Units";
			var lv_objnr_desc:String = "Object number at item level";
			var lv_obser:String ="Text_Line";
			var lv_zz_pump_assing:String = "Assignent pump";
			var lv_sign1_desc:String = "Container ID";
			var lv_Exit1_desc:String = "Vehicle Plate 1";
			
			lv_label = 	lv_datgb_desc + "\n" +
				lv_ezeit_desc + "\n" + 
				lv_uatbg_desc + "\n" + 
				lv_lddat_desc + "\n" + 
				lv_lduhr_desc + "\n" + 
				lv_ualbg_desc + "\n" + 
				lv_werks_desc + "\n" + 
				lv_name_desc + "\n" + 
				lv_zzwerkool_desc + "\n" + 
				lv_vbeln_desc + "\n" + 
				lv_posnr_desc + "\n" + 
				lv_namecust_desc + "\n" + 
				lv_nameobj_desc + "\n" + 
				lv_arktx_desc + "\n" + 
				lv_matnr_desc + "\n" + 
				lv_atwrt_desc + "\n" + 
				lv_kwmeng_desc + "\n" + 
				lv_objnr_desc + "\n" + 
				lv_obser + "\n" + 
				lv_zz_pump_assing + "\n" +
				lv_sign1_desc + "\n" +
				lv_Exit1_desc;
			
			
			
			
			
			
			/* for(var i:Number = 0; i<dataSourceAnOrderDetail.length; i++){
			ob_detail = dataSourceAnOrderDetail.getItemAt(i);
			}
			
			for(var i:Number = 0; i<dataSourceLinesDetail.length; i++){
			ob_line = dataSourceLinesDetail.getItemAt(i);
			} */
			
			return lv_label;
		}
		public static function getValuesOrderDetail(dataSourceAnOrderDetail:ArrayCollection):String{
			var lv_valuesDetail:String = "";
			
			var ob_detail:Object = null;
			var ob_line:Object = null;
			
			var lv_datgb:String = "";
			var lv_ezeit:String = "";
			var lv_uatbg:String = "";
			var lv_lddat:String = "";
			var lv_lduhr:String = "";
			var lv_ualbg:String = "";
			var lv_werks:String = "";
			var lv_name:String  = "";
			var lv_zzwerkool:String = "";
			var lv_vbeln:String = "";
			var lv_posnr:String = "";
			var lv_namecust:String = "";
			var lv_nameobj:String = "";
			var lv_arktx:String = "";
			var lv_matnr:String = "";
			var lv_atwrt:String = "";
			var lv_kwmeng:String = "";
			var lv_objnr:String = "";
			var lv_obser:String ="";
			var lv_zz_pump_assing:String = "";
			var lv_sign1:String = "";
			var lv_Exit1:String = "";
			
			if(dataSourceAnOrderDetail.length!=0){
				//Alert.show();
				ob_detail = dataSourceAnOrderDetail.getItemAt(0);
				//Alert.show(ReflectionHelper.object2XML(ob_detail));
			}else{
				//Alert.show("No hay data para mostrar");
			}
			
			if(ob_detail["DATGB"]!=null){
				lv_datgb = ob_detail["DATGB"].toString();
			}
			
			if(ob_detail["EZEIT"]!=null){
				lv_ezeit = ob_detail["EZEIT"].toString();
			}
			
			if(ob_detail["LDDAT"]!=null){
				lv_lddat = ob_detail["LDDAT"].toString();
			}
			
			if(ob_detail["LDUHR"]!=null){
				lv_lduhr = ob_detail["LDUHR"].toString();
			}
			
			if( ob_detail["UALBG"]!=null){
				lv_ualbg =  ob_detail["UALBG"].toString();
			}
			
			//g1.crearColumna(
			
			if( ob_detail["WERKS"]!=null){
				lv_ualbg = ob_detail["WERKS"].toString();
			}
			
			if(ob_detail["ZZ_WERKOOL"]!=null){
				lv_zzwerkool = ob_detail["ZZ_WERKOOL"].toString();
			}
			
			if(ob_detail["VBELN"]!=null){
				lv_vbeln = ob_detail["VBELN"].toString();
			}
			
			if(ob_detail["POSNR"]!=null){
				lv_posnr = ob_detail["POSNR"].toString();
			}
			
			if(ob_detail["NAMEOBJ"]!=null){
				lv_nameobj = ob_detail["NAMEOBJ"].toString();
			}
			
			if(ob_detail["MATNR"]!=null){
				lv_matnr = ob_detail["MATNR"].toString();
			}
			
			if(ob_detail["ATWRT"]!=null){
				lv_atwrt = ob_detail["ATWRT"].toString();
			}
			
			if(ob_detail["KWMENG"]!=null){
				lv_kwmeng = ob_detail["KWMENG"].toString();
			}
			
			if(ob_detail["OBJNR"]!=null){
				lv_objnr = ob_detail["OBJNR"].toString();
			}
			
			lv_valuesDetail = 	lv_datgb + "\n" +
				lv_ezeit + "\n" + 
				lv_uatbg + "\n" + 
				lv_lddat + "\n" + 
				lv_lduhr + "\n" + 
				lv_ualbg + "\n" + 
				lv_werks + "\n" + 
				lv_name + "\n" + 
				lv_zzwerkool + "\n" + 
				lv_vbeln + "\n" + 
				lv_posnr + "\n" + 
				lv_namecust + "\n" + 
				lv_nameobj + "\n" + 
				lv_arktx + "\n" + 
				lv_matnr + "\n" + 
				lv_atwrt + "\n" + 
				lv_kwmeng + "\n" + 
				lv_objnr + "\n" + 
				lv_obser + "\n" + 
				lv_zz_pump_assing + "\n" +
				lv_sign1 + "\n" +
				lv_Exit1;
			
			return lv_valuesDetail;
		}
		public static function getObsOrderDetail(dataSourceLinesDetail:ArrayCollection):String{
			var ob_lines:Object = null;					//Objeto Línea
			var num_reg:Number = 0;						//Número Registros 
			var	lv_obsDetail:String = "";				//Variable de retorno
			
			//valida que haya datos para leer en el nodo lines
			if(dataSourceLinesDetail.length!=0){
				num_reg = dataSourceLinesDetail.length;
				//Recorre las lineas
				for(var i:Number = 0; i<num_reg; i++){
					ob_lines = dataSourceLinesDetail.getItemAt(i);
					//Alert.show(ob_lines.toString());
					//Alert.show(ReflectionHelper.object2XML(ob_lines));
					//Valida que el campo no sea null
					//Alert.show(ReflectionHelper.object2XML(ob_lines));
					if(ob_lines["TDLINE"]!=null){
						lv_obsDetail = lv_obsDetail + ob_lines["TDLINE"].toString();
					}						
				}
				//ob_lines = dataSourceLinesDetail.getItemAt(0);
			}else{
				//Alert.show("No hay data en lines para mostrar");
			}
			return lv_obsDetail;
		}
		
		/************************optimizacion de codigo: cambio los datos a mostrar en el contenido de full detail ***************************************/
		public static function fullDetailField():String{
			var modelTag:tagsSingleton = tagsSingleton.getInstance();
			var type:String 		= modelTag.internalComm.type+'\n';
			var numOrder:String 	= modelTag.internalComm.numberOrder+'\n';
			var numLoad:String 		= modelTag.internalComm.numberLoad+'\n';
			
			var loadNumber:String 		= "LOAD NUMBER"+'\n';
			var etenr:String 		= "ETENR"+'\n';
			
			var message:String		= modelTag.order.message+'\n';
			var retCode:String		= modelTag.internalComm.retCode+ '\n';
			var priority:String 	= modelTag.internalComm.priority + '\n';
			var product:String 		= removeCero(modelTag.internalComm.product) + '\n';
			var element:String 		= modelTag.internalComm.elementColar+'\n';
			var metUnload:String 	= modelTag.internalComm.dischargMethod+'\n';
			var frecuency:String 	= modelTag.internalComm.frequency + '\n';
			var optPlant:String 	= modelTag.order.optPlant+'\n';
			var pump:String 	    = modelTag.internalComm.pump+'\n';
			var pumpMax:String 	    = modelTag.internalComm.pumpMax+'\n';
			var pipes:String 		= modelTag.internalComm.pipe+'\n';
			var fix:String			= modelTag.order.fix+'\n';
			var deliveryTime:String = modelTag.internalComm.deliveryTime + '\n';
			var loadingTime:String 	= modelTag.internalComm.loadingTime + '\n';
			
			var numClient:String 	= modelTag.internalComm.clientName +'\n\n';
			var numFront:String 	= modelTag.internalComm.frontName +'\n';
			
			return type+numOrder.toUpperCase()+numLoad.toUpperCase()+loadNumber+etenr+message.toUpperCase()+retCode.toUpperCase()+priority.toUpperCase()+product.toUpperCase()+element.toUpperCase()+metUnload.toUpperCase()+frecuency.toUpperCase()+optPlant.toUpperCase()+pump.toUpperCase()+pumpMax.toUpperCase()+pipes.toUpperCase()+fix.toUpperCase()+deliveryTime.toUpperCase()+loadingTime.toUpperCase()+numClient.toUpperCase()+numFront.toUpperCase();
		}
		
		public static function fullDetailResult(resultFullDetail:ArrayCollection, id:String):String{
			var result:Object
			for(var i:int=0; i<resultFullDetail.length; i++){
				var re:Object =resultFullDetail.getItemAt(i);
				var val:String = String(re['VBELN'])+String(re['POSNR']);
				if( val == id){
					result = resultFullDetail.getItemAt(i);
					break
				}
			}		
			var devTime:String
			if(result['EZEIT'] != '00:00:00'){
				devTime = result['EZEIT'];
			}else{
				devTime = result['UATEN'];
			}			
			//return result['VBELN']+'\n'+result['POSNR']+'\n'+result['ZZINFORMATION']+'\n'+result['ZZRET_CODE']+'\n'+result['ZZ_AUART']+'\n'+result['ZZ_PRIORITY']+'\n'+result['MATNR']+'\n'+result['ZZ_ECOLAR']+'\n'+result['ZZ_UNLD_MTHD']+'\n'+result['ZZ_FREQUENCY']+'\n'+result['MATNR']+'\n'+result['ZZFIX_FLAG']+'\n'+devTime+'\n'+result['LDUHR']+'\n'+result['ZZ_CUSTOMER_NO']+' '+result['ZZ_CUSTOMER_DES']+'\n'+result['ZZDEL_ADDRESS']//+'\n'+result['ZZFIX_FLAG']+'\n'+devTime+'\n'+result['UALBG']//+result['ZZ_INFO_INTERN']
			var optPlant:String = result['ZZ_WERKOOL']
			if(optPlant == ''){
				optPlant = result['WERKS']
			}
			var fix:String = '';
			if(result['ZZFIX_FLAG'] != ''){
				fix = '*';
			}
			//return result['ZZ_AUART_DESC']+'\n'+result['VBELN']+'\n'+result['POSNR']+'\n'+ result['RETENTION_CODE_DESC'] +'\n'+result['ZZRET_CODE']+'\n'+result['PRIORITY_DESC']+'\n'+result['MATNR']+'\n'+result['ZZ_ECOLAR']+'\n'+result['ZZ_UNLD_MTHD']+'\n'+result['ZZ_FREQUENCY']+'min \n'+ optPlant +'\n'+ result['REQ_PUMP_TYPE_DESC'] +'\n'+result['ZZ_MAX_PUMP_TY']+'\n'+ result['ZZ_PIPE_DESC'] +'\n'+ fix +'\n'+devTime + '\n'+result['LDUHR']+'\n'+result['ZZ_CUSTOMER_NO']+' '+result['ZZ_CUSTOMER_DES']+'\n\n'+result['ZZDEL_ADDRESS']//+'\n'+result['ZZFIX_FLAG']+'\n'+devTime+'\n'+result['UALBG']//+result['ZZ_INFO_INTERN']
			return result['ZZ_AUART_DESC']+'\n'+removeCero(result['VBELN'])+'\n'+result['POSNR']+'\n'+result['LOAD']+'\n'+result['ETENR']+'\n'+ result['ZZ_MESSAGE'] +'\n'+result['RETENTION_CODE_DESC']+'\n'+result['PRIORITY_DESC']+'\n'+result['TECHNICAL_DESC']+'\n'+result['ZZ_ECOLAR']+'\n'+result['ZZ_UNLD_MTHD']+'\n'+result['ZZ_FREQUENCY']+' min \n'+ optPlant +'\n'+ result['REQ_PUMP_TYPE_DESC'] +'\n'+result['ZZ_MAX_PUMP_DESC']+'\n'+ result['ZZ_PIPE_DESC'] +'\n'+ fix +'\n'+devTime + '\n'+result['LDUHR']+'\n'+removeCero(result['ZZ_CUSTOMER_NO'])+' '+result['ZZ_CUSTOMER_DES']+'\n\n'+removeCero(result['ZZDEL_ADDRESS'])//+'\n'+result['ZZFIX_FLAG']+'\n'+devTime+'\n'+result['UALBG']//+result['ZZ_INFO_INTERN']
		}
		public static function fullDetailComentary(resultFullDetail:ArrayCollection, id:String):String{
			var result:Object
			for(var i:int=0; i<resultFullDetail.length; i++){
				var re:Object =resultFullDetail.getItemAt(i);
				var val:String = String(re['VBELN'])+String(re['POSNR']);
				if( val == id){
					result = resultFullDetail.getItemAt(i);
					break
				}
			}
			return result['ZZ_INFO_INTERN'];
		}
		/************************optimizacion de codigo: cambio los datos a mostrar en el contenido de full detail /***************************************/
		public static function normalDetailField(result:Object):String{
			var modelTag:tagsSingleton = tagsSingleton.getInstance();
			var numOrder:String 	= modelTag.order.number + '\n';
			var numLoad:String 		= modelTag.order.numberLoad + '\n';
			var etenr:String 		= "ETNR" + '\n';
			var status:String 		= modelTag.order.status + '\n';
			var numFront:String 	= modelTag.internalComm.frontName +'\n';
			var numClient:String	= modelTag.internalComm.clientName+'\n';
			var product:String 		= removeCero(modelTag.order.product) +'\n';				
			var deliveryTime:String = modelTag.order.deliveryTime + '\n';
			var loadingTime:String 	= modelTag.order.loadingTime + '\n';
			var fix:String 			= modelTag.order.fix.toUpperCase()+'\n';
			var message:String 		= '\n'+modelTag.order.message.toUpperCase()+'\n';
			var optPlant:String 	= modelTag.order.optPlant.toUpperCase()+'\n';
			
			var deliveryDate:String = modelTag.internalComm.deliveryDate+'\n';
			var requiredDate:String = modelTag.internalComm.requiredDate+'\n';
			var requiredTime:String = modelTag.internalComm.requiredTime+'\n';
			var waitVolume:String = modelTag.internalComm.queuWait+'\n';
			
			var space:String = '';
			if(removeCero(result['STATUS_DESC']).length>23){
				space = '\n';
			}
			var loadNumber:String="LOAD NUMBER"+'\n';
			
			
			return numLoad.toUpperCase()+loadNumber+etenr+status.toUpperCase()+space.toUpperCase()+product.toUpperCase()+message.toUpperCase()+deliveryDate.toUpperCase()+deliveryTime.toUpperCase()+loadingTime.toUpperCase()+requiredDate.toUpperCase()+requiredTime.toUpperCase()+waitVolume.toUpperCase()+fix.toUpperCase()+optPlant.toUpperCase()+numFront.toUpperCase();
		}
		public static function normalDetailResult(result:Object):String{
			//trace(result)
			var devTime:String
			if(result['EZEIT'] != '00:00:00'){
				devTime = result['EZEIT'];
			}else{
				devTime = result['UATEN'];
			}	
			var customer:String = removeCero(result['ZZDEL_ADDRESS']);//removeCero(result['ZZ_CUSTOMER_NO']) + ' ' + result['ZZ_CUSTOMER_DES']
			var optPlant:String = result['ZZ_WERKOOL']
			if(optPlant == ''){
				optPlant = result['WERKS']
			}
			var fix:String = '';
			if(result['ZZFIX_FLAG'] != ''){
				fix ='*';
			}
			var zzreqdate:String="";
			if(result['ZZREQDATE'] != null){
				zzreqdate+=result['ZZREQDATE'].date.toString()+"/";
				zzreqdate+=(result['ZZREQDATE'].month+1).toString()+"/";
				zzreqdate+=result['ZZREQDATE'].fullYear.toString();
			}
			var deliveryDate:String="";
			if(result['EDATU']!=null){
				deliveryDate+=result['EDATU'].date.toString()+"/";
				deliveryDate+=(result['EDATU'].month+1).toString()+"/";
				deliveryDate+=result['EDATU'].fullYear.toString();	
			}
			
			var thevolume:String="";
			if (result['TOTAL_VOL']!="0"){
				thevolume=result['TOTAL_VOL'];
			}
			var space:String="";
			if(removeCero(result['TECHNICAL_DESC']).length<26){
				space = '\n';
			}
			
			return result['POSNR']+'\n'+result['LOAD']+'\n'+result['ETENR']+'\n'+result['STATUS_DESC']+'\n'+removeCero(result['TECHNICAL_DESC'])+'\n'+space+ result['ZZ_MESSAGE'] +'\n'+deliveryDate+'\n'+devTime+'\n'+result['UALBG']+'\n'+zzreqdate+'\n'+result['ZZREQTIME']+'\n'+thevolume+'\n' +fix + '\n\n' + optPlant + '\n'+customer;
		}
		public static function removeCero(data:String):String{
			var pos:int = 0;
			for (var i:int; i< data.length; i++){
				if(data.charAt(i) != '0'){
					pos = i;
					break
				}
			}
			return data.slice(pos);
		}
		/*********************************** optimizacion de codigo: cambio los datos a mostrar en el contenido de normal detail ***************************************/
		
	}
	
}