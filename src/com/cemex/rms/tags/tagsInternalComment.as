package com.cemex.rms.tags
{
	[Bindable]
	public class tagsInternalComment
	{
		public var type:String = '';
		public var property:String = '';
		public var result:String = '';
		public var numberOrder:String = '';
		public var numberLoad:String = '';
		public var priority:String = '';
		public var product:String = '';
		public var elementColar:String = '';
		public var dischargMethod:String = '';
		public var frequency:String = '';
		public var pipes:String = '';
		public var deliveryTime:String = '';
		public var loadingTime:String = '';
		public var clientName:String = '';
		public var frontName:String = '';
		public var internalComentary:String = '';
		public var buttonLabel:String = '';
		public var retCode:String = '';
		public var pump:String = '';
		public var pumpMax:String = '';
		public var pipe:String = '';
		
		public var deliveryDate:String='';
		public var requiredDate:String='';
		public var requiredTime:String='';
		public var queuWait:String='';
		
		public function tagsInternalComment()
		{
		}
	}
}