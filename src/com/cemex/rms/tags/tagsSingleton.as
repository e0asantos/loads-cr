package com.cemex.rms.tags
{
	[Bindable]
	public class tagsSingleton
	{
		public var order:tagsOrder = new tagsOrder();
		public var header:tagsHeader = new tagsHeader();
		public var itemSel:tagItemSelect = new tagItemSelect();
		public var internalComm:tagsInternalComment = new tagsInternalComment();
		public var alert:tagsAlerts = new tagsAlerts();
		public var push:tagsPush = new tagsPush();
		
		static private var _instance:tagsSingleton = null;	
		static public function getInstance():tagsSingleton{
			if(_instance == null){
				_instance = new tagsSingleton();
			}
			return _instance;
		}
	}
}